FROM ubuntu:focal

ARG VERSION

LABEL \
  org.opencontainers.image.title="Tailon" \
  org.opencontainers.image.authors="Vincenzo Santucci - Sandro Spadaro" \
  org.opencontainers.image.vendor="" \
  org.opencontainers.image.url="local" \
  org.opencontainers.image.source="https://gitlab.com/howtodoit/kubernetes/tools/tailon.git" \
  org.opencontainers.image.version="$VERSION" \
  vendor="" \
  name="Tailon" \
  version="$VERSION" \
  summary="Tailon Log Web Console" \
  description="Tailon Log Web Console"

ADD https://github.com/gvalkov/tailon/releases/download/v1.1.0/tailon_1.1.0_linux_amd64.tar.gz tailon.tar.gz

RUN tar -xvf tailon.tar.gz && install tailon /usr/bin

WORKDIR /root

ENTRYPOINT ["/usr/bin/tailon"]